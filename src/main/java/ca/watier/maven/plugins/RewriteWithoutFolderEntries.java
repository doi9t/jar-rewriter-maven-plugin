/*
 *    Copyright 2014 - 2020 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.maven.plugins;

import org.apache.commons.io.IOUtils;
import org.apache.maven.plugins.annotations.Mojo;

import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

@Mojo(name = "remove-folders-entries")
public class RewriteWithoutFolderEntries extends AbstractCopyOperationAndReplaceFinalJar {
    private static final String MSG_REMOVED_DIR_ENTRY = "Removed the directory entry -> %s";

    @Override
    protected String operationMainDescription() {
        return "Removing folder entries the the final jar";
    }

    @Override
    protected void writeReWriteEntryToCopyZip(ZipOutputStream copyZip, ZipFile zipToCopy, ZipEntry zipEntry) throws IOException {
        String entryName = zipEntry.getName();
        byte[] entryBytes = IOUtils.toByteArray(zipToCopy.getInputStream(zipEntry));

        if (zipEntry.isDirectory()) {
            getLog().debug(String.format(MSG_REMOVED_DIR_ENTRY, entryName));
            return;
        }

        ZipEntry zipEntryToWrite = new ZipEntry(entryName);
        copyZip.putNextEntry(zipEntryToWrite);
        copyZip.write(entryBytes, 0, entryBytes.length);
    }
}