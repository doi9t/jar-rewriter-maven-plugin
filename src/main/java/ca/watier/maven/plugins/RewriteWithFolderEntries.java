/*
 *    Copyright 2014 - 2020 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.maven.plugins;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugins.annotations.Mojo;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

@Mojo(name = "add-folders-entries")
public class RewriteWithFolderEntries extends AbstractCopyOperationAndReplaceFinalJar {
    private static final String MSG_CREATED_NEW_DIR_ENTRY = "Created the new directory entry -> %s";
    private final Set<String> createdFolderStructures = new HashSet<>();

    @Override
    protected String operationMainDescription() {
        return "Adding folder entries the final jar";
    }

    @Override
    protected void writeReWriteEntryToCopyZip(ZipOutputStream copyZip, ZipFile zipToCopy, ZipEntry zipEntry) throws IOException {
        String entryName = zipEntry.getName();
        byte[] entryBytes = IOUtils.toByteArray(zipToCopy.getInputStream(zipEntry));

        String[] directoriesAsArray = getEntryDirectories(entryName, zipEntry.isDirectory());
        int numberOfDirectories = ArrayUtils.getLength(directoriesAsArray);

        if (numberOfDirectories > 0) {
            addDirectoryEntries(zipToCopy, copyZip, directoriesAsArray);
        }

        ZipEntry zipEntryToWrite = new ZipEntry(entryName);
        copyZip.putNextEntry(zipEntryToWrite);
        copyZip.write(entryBytes, 0, entryBytes.length);
    }

    private String[] getEntryDirectories(String entryName, boolean isDirectory) {
        String[] directories;
        if (isDirectory) {
            directories = StringUtils.split(entryName, '/');
        } else {
            String path = FilenameUtils.getPathNoEndSeparator(entryName);
            directories = StringUtils.split(path, '/');
        }
        return directories == null ? new String[0] : directories;
    }

    private void addDirectoryEntries(ZipFile zipToCopy, ZipOutputStream copyZip, String[] directoriesAsArray) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        for (String currentDirectory : directoriesAsArray) {
            stringBuilder.append(currentDirectory).append('/');
            String name = stringBuilder.toString();

            if (createdFolderStructures.add(name) && zipToCopy.getEntry(name) == null) {
                getLog().debug(String.format(MSG_CREATED_NEW_DIR_ENTRY, name));
                ZipEntry zipEntryToWrite = new ZipEntry(name);
                copyZip.putNextEntry(zipEntryToWrite);
            }
        }
    }
}