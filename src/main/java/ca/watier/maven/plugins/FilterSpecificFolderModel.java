package ca.watier.maven.plugins;

import org.apache.maven.plugins.annotations.Parameter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FilterSpecificFolderModel {
    @Parameter(property = "entriesByRegex", required = true)
    private final List<String> entriesByRegex = new ArrayList<>();

    @Parameter(property = "name", required = true)
    private String name;

    public FilterSpecificFolderModel() {
    }

    public FilterSpecificFolderModel(String name, List<String> entriesByRegex) {
        this.name = name;

        if (entriesByRegex != null && entriesByRegex.size() > 0) {
            this.entriesByRegex.addAll(entriesByRegex);
        }
    }

    public List<String> getEntriesByRegex() {
        return Collections.unmodifiableList(entriesByRegex);
    }

    public void setEntriesByRegex(List<String> entriesByRegex) {
        if (entriesByRegex != null && entriesByRegex.size() > 0) {
            this.entriesByRegex.addAll(entriesByRegex);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
