/*
 *    Copyright 2014 - 2020 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.maven.plugins;

import org.apache.commons.io.IOUtils;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

@Mojo(name = "filter-specific-folder")
public class RewriteFilterSpecificFolder extends AbstractCopyOperationAndReplaceFinalJar {

    private static final String MSG_REMOVED_ENTRY = "Removed the entry -> %s";
    private static final String MSG_KEPT_ENTRY = "Kept the entry -> %s";
    private final List<Pattern> compiledSpecificEntriesByRegex = new ArrayList<>();

    @Parameter(property = "filterSpecificFolder", required = true)
    protected FilterSpecificFolderModel filterSpecificFolder;

    private Path wantedFolder;

    @Override
    protected String operationMainDescription() {
        return "Filtering specific entries of the final jar";
    }

    @Override
    protected void preCopyEvent() {
        getLog().debug("Compiling the regex patterns [...]");
        wantedFolder = Paths.get(filterSpecificFolder.getName());
        for (String entriesByRegex : filterSpecificFolder.getEntriesByRegex()) {

            String entryRegex = entriesByRegex;

            // Remove, since the java paths in the jar doesn't begin with a slash
            if (entriesByRegex.startsWith("/")) {
                entryRegex = entriesByRegex.substring(1);
            }

            compiledSpecificEntriesByRegex.add(Pattern.compile(entryRegex));
        }
    }

    @Override
    protected void writeReWriteEntryToCopyZip(ZipOutputStream copyZip, ZipFile zipToCopy, ZipEntry zipEntry) throws IOException {
        String entryName = zipEntry.getName();
        byte[] entryBytes = IOUtils.toByteArray(zipToCopy.getInputStream(zipEntry));

        Path currentPath = Paths.get(entryName);

        // It's the folder or an element outside
        if (!currentPath.startsWith(wantedFolder) || currentPath.equals(wantedFolder)) {
            write(copyZip, entryName, entryBytes);
            return;
        }

        String relativizedPath = wantedFolder.relativize(currentPath).toString();

        for (Pattern specificEntriesByRegex : compiledSpecificEntriesByRegex) {
            if (specificEntriesByRegex.matcher(relativizedPath).find()) {
                getLog().debug(String.format(MSG_KEPT_ENTRY, relativizedPath));
                write(copyZip, entryName, entryBytes);
                break;
            } else {
                getLog().debug(String.format(MSG_REMOVED_ENTRY, relativizedPath));
            }
        }
    }

    private void write(ZipOutputStream copyZip, String entryName, byte[] entryBytes) throws IOException {
        ZipEntry zipEntryToWrite = new ZipEntry(entryName);
        copyZip.putNextEntry(zipEntryToWrite);
        copyZip.write(entryBytes, 0, entryBytes.length);
    }
}