/*
 *    Copyright 2014 - 2020 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.maven.plugins;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

@Mojo(name = "add-specific-entries")
public class RewriteAddSpecificEntries extends AbstractCopyOperationAndReplaceFinalJar {
    public static final String MSG_FILE_DO_NOT_EXIST = "The file %s doesn't exist!";
    public static final String MSG_ERROR_COPING_FILE_ENTRY = "Unable to copy the file %s !";
    public static final String SLASH = "/";
    public static final String MSG_ADDED_FILE_ENTRY = "Coping the file to the copy jar -> %s";
    public static final String MSG_ADDED_FOLDER_ENTRY = "Coping the folder to the copy jar -> %s";
    private static final String MSG_ADDED_ENTRY = "Added the entry -> %s";
    @Parameter(property = "addFolderEntries", defaultValue = "true")
    protected boolean addFolderEntries;

    @Parameter(property = "addSpecificEntries", required = true)
    protected Set<String> addSpecificEntries;

    @Parameter(defaultValue = "${project.basedir}")
    private File projectBaseDirectory;

    private void addAllChildFilesAndDirectoriesToZip(ZipOutputStream copyZip, String entryName, File specificFile) throws IOException {
        if (specificFile.isDirectory()) {
            String folderName = buildFolderName(entryName);
            File[] childrenFiles = specificFile.listFiles();
            boolean isEmptyFolder = childrenFiles == null || childrenFiles.length == 0;

            if (addFolderEntries || isEmptyFolder) {
                addFolderEntryToZip(copyZip, folderName);
                getLog().debug(String.format(MSG_ADDED_FOLDER_ENTRY, folderName));
            }

            if (isEmptyFolder) {
                return;
            }

            for (File childFile : childrenFiles) {
                String childName = buildChildFileName(folderName, childFile);
                addAllChildFilesAndDirectoriesToZip(copyZip, childName, childFile);
            }

        } else {
            byte[] entryBytes = FileUtils.readFileToByteArray(specificFile);
            getLog().debug(String.format(MSG_ADDED_FILE_ENTRY, entryName));
            addFileEntryToZip(copyZip, entryName, entryBytes);
        }
    }

    private String buildChildFileName(String folderName, File child) {
        return folderName + child.getName();
    }

    private String buildFolderName(String addSpecificEntry) {
        if (!StringUtils.endsWith(addSpecificEntry, SLASH)) {
            return addSpecificEntry + "/";
        } else {
            return addSpecificEntry;
        }
    }

    private void addFolderEntryToZip(ZipOutputStream copyZip, String entryName) throws IOException {
        ZipEntry zipEntryToWrite = new ZipEntry(entryName);
        copyZip.putNextEntry(zipEntryToWrite);
    }

    @Override
    protected String operationMainDescription() {
        return "Removing specific entries of the final jar";
    }

    @Override
    protected void afterCopy(ZipOutputStream copyZip) {
        getLog().debug("Coping the file(s) to the copy jar [...]");

        for (String addSpecificEntry : addSpecificEntries) {
            File specificFile = new File(projectBaseDirectory, addSpecificEntry);

            if (specificFile.exists()) {
                try {
                    addAllChildFilesAndDirectoriesToZip(copyZip, addSpecificEntry, specificFile);
                } catch (IOException e) {
                    getLog().debug(String.format(MSG_ERROR_COPING_FILE_ENTRY, specificFile.getName()));
                }
                getLog().debug(String.format(MSG_ADDED_ENTRY, specificFile.getName()));
            } else {
                getLog().warn(String.format(MSG_FILE_DO_NOT_EXIST, specificFile.getName()));
            }
        }

        getLog().debug("Done!");
    }

    @Override
    protected void writeReWriteEntryToCopyZip(ZipOutputStream copyZip, ZipFile zipToCopy, ZipEntry zipEntry) throws IOException {
        String entryName = zipEntry.getName();
        byte[] entryBytes = IOUtils.toByteArray(zipToCopy.getInputStream(zipEntry));

        addFileEntryToZip(copyZip, entryName, entryBytes);
    }

    private void addFileEntryToZip(ZipOutputStream copyZip, String entryName, byte[] entryBytes) throws IOException {
        ZipEntry zipEntryToWrite = new ZipEntry(entryName);
        copyZip.putNextEntry(zipEntryToWrite);
        copyZip.write(entryBytes, 0, entryBytes.length);
    }
}