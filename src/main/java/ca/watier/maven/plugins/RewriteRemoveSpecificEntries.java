/*
 *    Copyright 2014 - 2020 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.maven.plugins;

import org.apache.commons.io.IOUtils;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

@Mojo(name = "remove-specific-entries")
public class RewriteRemoveSpecificEntries extends AbstractCopyOperationAndReplaceFinalJar {
    private static final String MSG_INVALID_PATTERN = "%s is not a valid regex pattern, skipping it!";
    private static final String MSG_REMOVED_ENTRY = "Removed the entry -> %s";

    @Parameter(property = "removeSpecificEntriesByRegex", required = true)
    protected Set<String> removeSpecificEntriesByRegex;
    protected List<Pattern> compiledSpecificEntriesByRegex = new ArrayList<>();

    @Override
    protected String operationMainDescription() {
        return "Removing specific entries of the final jar";
    }

    @Override
    protected void preCopyEvent() {
        getLog().debug("Compiling the regex patterns [...]");
        for (String specificEntriesByRegex : removeSpecificEntriesByRegex) {
            try {
                String entryRegex = specificEntriesByRegex;

                // Remove, since the java paths in the jar doesn't begin with a slash
                if (entryRegex.startsWith("/")) {
                    entryRegex = specificEntriesByRegex.substring(1);
                }

                Pattern compiledPattern = Pattern.compile(entryRegex);
                compiledSpecificEntriesByRegex.add(compiledPattern);
            } catch (PatternSyntaxException ex) {
                getLog().warn(String.format(MSG_INVALID_PATTERN, specificEntriesByRegex));
            }
        }
        getLog().debug("Done!");
    }

    @Override
    protected void writeReWriteEntryToCopyZip(ZipOutputStream copyZip, ZipFile zipToCopy, ZipEntry zipEntry) throws IOException {
        String entryName = zipEntry.getName();
        byte[] entryBytes = IOUtils.toByteArray(zipToCopy.getInputStream(zipEntry));

        for (String entry : removeSpecificEntriesByRegex) {
            if (entryName.matches(entry)) {
                getLog().debug(String.format(MSG_REMOVED_ENTRY, entryName));
                return;
            }
        }

        ZipEntry zipEntryToWrite = new ZipEntry(entryName);
        copyZip.putNextEntry(zipEntryToWrite);
        copyZip.write(entryBytes, 0, entryBytes.length);
    }
}