/*
 *    Copyright 2014 - 2020 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.maven.plugins;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public abstract class AbstractCopyOperationAndReplaceFinalJar extends AbstractMojo {
    private static final String MSG_DELETING_FILE = "Deleting %s";
    private static final String MSG_RENAMING_FILE_INTO = "Renaming %s into %s";
    private static final String MSG_REWRITING_JAR = "Rewriting %s";
    private static final String COPY_NAME_PART = "_copy";
    private static final String JAR_EXT = ".jar";

    @Parameter(defaultValue = "${project.build.finalName}")
    protected String jarToRewrite;

    @Parameter(defaultValue = "${project.build.directory}")
    protected File buildDirectory;

    @Override
    public final void execute() {
        String copyJarName = copyJarName();
        String finalJarName = finalJarName();

        getLog().info(String.format(MSG_REWRITING_JAR, finalJarName));

        File jarFile = new File(buildDirectory, finalJarName);
        File copyJarFile = new File(buildDirectory, copyJarName);

        String operationLogDescription = operationMainDescription();

        if (StringUtils.isNotBlank(operationLogDescription)) {
            getLog().info(String.format("\t== %s ==", operationLogDescription));
        }

        preCopyEvent();
        copyZip(copyJarName, finalJarName, jarFile, copyJarFile);
        postCopyEvent();
    }

    private String copyJarName() {
        return jarToRewrite + COPY_NAME_PART + JAR_EXT;
    }

    protected String finalJarName() {
        return jarToRewrite + JAR_EXT;
    }

    protected abstract String operationMainDescription();

    protected void preCopyEvent() {
    }

    protected void copyZip(String copyJarName, String finalJarName, File jarFile, File copyJarFile) {
        try (ZipOutputStream copyZip = new ZipOutputStream(new FileOutputStream(copyJarFile))) {
            ZipFile zipToCopy = new ZipFile(jarFile);
            Enumeration<? extends ZipEntry> entries = zipToCopy.entries();

            beforeCopy(copyZip);
            startCopy(copyZip, zipToCopy, entries);
            afterCopy(copyZip);
            replaceFinalZipWithCopy(copyJarName, finalJarName, jarFile, copyJarFile);

        } catch (IOException e) {
            getLog().error("Unable to read or copy the jar file !", e);
        }
    }

    protected void postCopyEvent() {
    }

    protected void beforeCopy(ZipOutputStream copyZip) {
    }

    private void startCopy(ZipOutputStream copyZip, ZipFile zipToCopy, Enumeration<? extends ZipEntry> entries) throws IOException {
        while (entries.hasMoreElements()) {
            writeReWriteEntryToCopyZip(copyZip, zipToCopy, entries.nextElement());
        }
    }

    protected void afterCopy(ZipOutputStream copyZip) {
    }

    private void replaceFinalZipWithCopy(String copyJarName, String finalJarName, File jarFile, File copyJarFile) {
        try {
            getLog().debug(String.format(MSG_DELETING_FILE, finalJarName));
            FileUtils.forceDelete(jarFile);
            getLog().debug(String.format(MSG_RENAMING_FILE_INTO, copyJarName, finalJarName));
            copyJarFile.renameTo(jarFile);
        } catch (IOException ex) {
            getLog().error("Unable to delete the file!", ex);
        }
    }

    protected abstract void writeReWriteEntryToCopyZip(ZipOutputStream copyZip, ZipFile zipToCopy, ZipEntry zipEntry) throws IOException;
}
