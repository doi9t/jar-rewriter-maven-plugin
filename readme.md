# jar-rewriter-maven-plugin 

A simple maven plugin to rewrite the jar and allow to customize it.

## Available plugin goals

### add-folders-entries
The goal creates missing folders entries in the JAR.

**Example**
```xml
<plugin>
    <groupId>ca.watier.maven.plugins</groupId>
    <artifactId>jar-rewriter-maven-plugin</artifactId>
    <version>X.Y.Z</version>
        <executions>
            <execution>
            <phase>package</phase>
            <goals>
                <goal>add-folders-entries</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```
### remove-folders-entries
The goal remove folders entries from the JAR (keep only file entries).

**Example**
```xml
<plugin>
    <groupId>ca.watier.maven.plugins</groupId>
    <artifactId>jar-rewriter-maven-plugin</artifactId>
    <version>X.Y.Z</version>
        <executions>
            <execution>
            <phase>package</phase>
            <goals>
                <goal>remove-folders-entries</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```
### remove-specific-entries
The goal removes any entries (folder or files) that are matching the [regex pattern](https://docs.oracle.com/en/java/javase/14/docs/api/java.base/java/util/regex/Pattern.html).

**Example**
```xml
<plugin>
    <groupId>ca.watier.maven.plugins</groupId>
    <artifactId>jar-rewriter-maven-plugin</artifactId>
    <version>X.Y.Z</version>
        <executions>
            <execution>
            <phase>package</phase>
            <goals>
                <goal>remove-specific-entries</goal>
            </goals>
            <configuration>
                <removeSpecificEntriesByRegex>
                    <entriesByRegex>.*directory.*</entriesByRegex> <!-- The tag can have any name and value -->
                    <entriesByRegex>.*\.png</entriesByRegex>
                </removeSpecificEntriesByRegex>
            </configuration>
        </execution>
    </executions>
</plugin>
```
### add-specific-entries
The goal adds external files / folders matching the path into the final JAR; the base path is the current project.

**Example**
```xml
<plugin>
    <groupId>ca.watier.maven.plugins</groupId>
    <artifactId>jar-rewriter-maven-plugin</artifactId>
    <version>X.Y.Z</version>
        <executions>
            <execution>
            <phase>package</phase>
                <goals>
                    <goal>add-specific-entries</goal>
                </goals>
                <configuration>
                    <addFolderEntries>true</addFolderEntries> <!-- Default value is true -->
                    <addSpecificEntries>
                        <entry>pom.xml</entry> <!-- The tag can have any name and value -->
                        <entry>target/classes</entry>
                        <entry>../externalProjectFile.xml</entry>
                    </addSpecificEntries>
                </configuration>
        </execution>
    </executions>
</plugin>
```
### filter-specific-folder
Filter the entry in a specific folder.

```xml
<plugin>
    <groupId>ca.watier.maven.plugins</groupId>
    <artifactId>jar-rewriter-maven-plugin</artifactId>
    <version>X.Y.Z</version>
        <executions>
            <execution>
            <phase>package</phase>
                <goals>
                    <goal>filter-specific-folder</goal>
                </goals>
                <configuration>
                    <filterSpecificFolder>
                        <name>users/group/admins</name>
                        <entriesByRegex>
                            <entry>^external/.*</entry>
                        </entriesByRegex>
                    </filterSpecificFolder>
                </configuration>
        </execution>
    </executions>
</plugin>
```